Summary:       Library to access the DWARF Debugging file format 
Name:          libdwarf
Version:       0.9.2
Release:       1%{?dist}
License:       LGPL-2.1-only AND BSD-2-Clause-FreeBSD
URL:           https://www.prevanders.net/dwarf.html
Source0:       https://www.prevanders.net/%{name}-%{version}.tar.xz
# https://github.com/davea42/libdwarf-code/blob/main/bugxml/data.txt
# https://github.com/davea42/libdwarf-code/commit/404e6b1b14f60c81388d50b4239f81d461b3c3ad
BuildRequires: gcc make python3

%description
Library to access the DWARF debugging file format which supports
source level debugging of a number of procedural languages, such as C, C++,
and Fortran.

%package devel
Summary:       Library and header files of libdwarf
License:       LGPL-2.1-only AND BSD-2-Clause-FreeBSD
Requires:      %{name} = %{version}-%{release}

%description devel
This package provides library and header files of libdwarf.

%package static
Summary:       Static libdwarf library
License:       LGPL-2.1-only AND BSD-2-Clause-FreeBSD
Requires:      %{name}-devel = %{version}-%{release}

%description static
This package provides static libdwarf library.

%package tools
Summary:       Tools for accessing DWARF debugging information
License:       GPL-2.0-only AND BSD-2-Clause-FreeBSD
Requires:      %{name} = %{version}-%{release}

%description tools
C++ version of dwarfdump (dwarfdump2) command-line utilities 
to access DWARF debug information.


%prep
%autosetup -p1


%build
%configure --enable-shared
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make_build


%install
%make_install

rm -f %{buildroot}/%{_libdir}/libdwarf.la

%check
%__make check


%files
%license src/lib/libdwarf/COPYING src/lib/libdwarf/LIBDWARFCOPYRIGHT src/lib/libdwarf/LGPL.txt
%doc src/lib/libdwarf/ChangeLog src/lib/libdwarf/README
%{_libdir}/libdwarf.so.0
%{_libdir}/libdwarf.so.0.*


%files static
%{_libdir}/libdwarf.a


%files devel
%doc doc/*.pdf
%{_includedir}/libdwarf-0
%{_libdir}/libdwarf.so
%{_libdir}/pkgconfig/libdwarf.pc


%files tools
%license src/bin/dwarfdump/COPYING src/bin/dwarfdump/DWARFDUMPCOPYRIGHT src/bin/dwarfdump/GPL.txt
%{_bindir}/dwarfdump
%{_datadir}/dwarfdump/dwarfdump.conf
%{_mandir}/man1/dwarfdump.1.gz


%changelog
* Tue Apr 23 2024 Upgrade Robot <upbot@opencloudos.tech> - 0.9.2-1
- [Type] security
- [Desc] Upgrade to version 0.9.2
- [Desc] Fix CVE-2024-31745.

* Thu Mar 21 2024 rockerzhu <rockerzhu@tencent.com> - 0.9.1-1
- Fix CVE-2024-2002.
- Upgrade to 0.9.1 because 0.5.0 is 2022.

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.0-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.0-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Dec 5 2022 cunshunxia <cunshunxia@tencent.com> - 0.5.0-1
- initial build
